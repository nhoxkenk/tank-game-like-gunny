using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fuel : MonoBehaviour
{
    [SerializeField] private Tank playerTank;
    public int startingFuel = 1000;
    public int fuelConsume;
    [SerializeField] private Slider fuelSlider;
    [SerializeField] private int currentFuel;

    private void Start()
    {
        fuelSlider = GetComponent<Slider>();
        currentFuel = startingFuel;
        fuelSlider.value = currentFuel;
    }

    private void Update()
    {
        ConsumeFuelWhenMovingTank();
    }

    private void ConsumeFuelWhenMovingTank()
    {
        if (playerTank.isMoving)
        {
            currentFuel -= fuelConsume;
            fuelSlider.value = currentFuel;
            if (currentFuel <= 0)
            {
                playerTank.haveFuelLeft = false;
                return;
            }
        }
    }

    public void ReturnFuelAfterOneRound()
    {
        currentFuel = startingFuel;
        fuelSlider.value = currentFuel;
    }

    public void SetConditionFuel()
    {
        playerTank.haveFuelLeft = true;
    }
}
