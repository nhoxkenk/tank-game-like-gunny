public enum GameState
{
     PlayerTurn,
     EnemyTurn,
     Decide,
     Victory,
     Lose
}
