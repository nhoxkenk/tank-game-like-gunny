using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem.OnScreen;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Button attackButton;
    [SerializeField] private TextMeshProUGUI angleText;
    [SerializeField] private TextMeshProUGUI powerText;
    [SerializeField] private OnScreenStick leftStick;
    [SerializeField] private OnScreenStick rightStick;
    [SerializeField]
    private GameObject victoryCanvas;
    [SerializeField]
    private GameObject loseCanvas;

    private void Awake()
    {
        GameManager.OnGameStateChanged += HandleOnGameStateChanged;
        TankRotateTurret.OnJoystickValueChanged += HandleOnAngleChange;
    }

    private void Update()
    {
        if(GameManager.instance.state == GameState.Victory)
        {
            victoryCanvas.SetActive(true);
        }
        else if (GameManager.instance.state == GameState.Lose)
        {
            loseCanvas.SetActive(true);
        }
    }

    private void OnDestroy()
    {
        GameManager.OnGameStateChanged -= HandleOnGameStateChanged;
        TankRotateTurret.OnJoystickValueChanged += HandleOnAngleChange;
    }

    private void HandleOnGameStateChanged(GameState state)
    {
        attackButton.interactable = state == GameState.PlayerTurn;  //Check if the state currently in is PlayerTurn
        leftStick.enabled = state == GameState.PlayerTurn;
        rightStick.enabled = state == GameState.PlayerTurn;
    }

    private void HandleOnAngleChange(int angle, float power)
    {
        angleText.text = angle.ToString();
        powerText.text = power.ToString();
    }

    public void OnAttackButtonPress()
    {
        GameManager.instance.UpdateGameState(GameState.EnemyTurn);
    }
}
