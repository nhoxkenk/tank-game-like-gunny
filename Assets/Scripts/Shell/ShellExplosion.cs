﻿using Unity.VisualScripting;
using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    public LayerMask tankMask;                        // Used to filter what the explosion affects, this should be set to "Players".
    public ParticleSystem explosionParticles;         // Reference to the particles that will play on explosion.
    public AudioSource explosionAudio;                // Reference to the audio that will play on explosion.
    public float maxDamage = 100f;                    // The amount of damage done if the explosion is centred on a tank.
    public float explosionForce = 1000f;              // The amount of force added to a tank at the centre of the explosion.
    public float maxLifeTime = 2f;                    // The time in seconds before the shell is removed.
    public float explosionRadius = 5f;                // The maximum distance away from the explosion tanks can be and are still affected.

    private Rigidbody shellRigidbody;

    private void Start()
    {
        shellRigidbody = GetComponent<Rigidbody>();
        Destroy(gameObject, maxLifeTime);
    }

    private void Update()
    {
        transform.forward = shellRigidbody.velocity;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Find all the tanks in an area around the shell and damage them.
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, tankMask);
        foreach (var col in colliders)
        {
            Rigidbody tankRigidbody = col.GetComponent<Rigidbody>();
            if (!tankRigidbody)
                continue;
            tankRigidbody.AddExplosionForce(explosionForce, transform.position, explosionRadius);
            TankHealth tankHealth = tankRigidbody.GetComponent<TankHealth>();
            if (!tankHealth)
                continue;
            float damage = CalculateDamage(tankRigidbody.position);
            tankHealth.TakeDamage(damage);
        }
        explosionParticles.transform.parent = null;
        explosionParticles.Play();
        explosionAudio.Play();
        Destroy(explosionParticles.gameObject, explosionParticles.main.duration);
        Destroy(gameObject);
    }


    private float CalculateDamage(Vector3 targetPosition)
    {
        // Calculate the amount of damage a target should take based on it's position.
        Vector3 explosionToTarget = targetPosition - transform.position;
        float explosionDistance = explosionToTarget.magnitude;
        // Calculate the proportion of the maximum distance (the explosionRadius) the target is away.
        float relativeDistance = (explosionRadius - explosionDistance) / explosionRadius;
        float damage = relativeDistance * maxDamage;
        damage = Mathf.Max(0f, damage);
        return damage;
    }
}