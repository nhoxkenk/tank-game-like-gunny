﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class TankShooting : MonoBehaviour
{
    public static TankShooting instance;
    private void Awake()
    {
        if(instance == null)
            instance = this;
        TankRotateTurret.OnJoystickValueChanged += HandleTankPowerForce;
    }

    private void OnDestroy()
    {
        TankRotateTurret.OnJoystickValueChanged -= HandleTankPowerForce;
    }

    public int playerNumber = 1;       
    public Rigidbody shellRigidbody;            
    public Transform fireTransform;    
    public Slider aimSlider;           
    public AudioSource shootingAudio;  
    public AudioClip chargingClip;     
    public AudioClip fireClip;         
    public float minLaunchForce = 15f; 
    public float maxLaunchForce = 30f; 
    public float maxChargeTime = 0.75f;
    
    public float CurrentLaunchForce { get; private set; }     

    private void OnEnable()
    {
        CurrentLaunchForce = minLaunchForce;
        aimSlider.value = minLaunchForce;
    }

    private void Update()
    {
        //// Track the current state of the fire button and make decisions based on the current launch force.
        //aimSlider.value = minLaunchForce;
        ////max force is exceed and shell hasn't been fired
        //if (currentLaunchForce >= maxLaunchForce && !fired)
        //{
        //    currentLaunchForce = maxLaunchForce;
        //    Fire();
        //}else //Start to hold the fire button 
        //if (Input.GetButtonDown(fireButton))
        //{
        //    fired = false;
        //    currentLaunchForce = minLaunchForce;

        //    shootingAudio.clip = chargingClip;
        //    shootingAudio.Play();
        //}else //Hold the fire button and shell hasn't fired
        //if(Input.GetButton(fireButton) && !fired)
        //{
        //    currentLaunchForce += chargeSpeed * Time.deltaTime;
        //    aimSlider.value = currentLaunchForce;
        //}else //Shell input is released and the shell hasn't launch
        //if(Input.GetButtonUp(fireButton) && !fired)
        //{
        //    Fire();
        //}
    }

    public void Fire()
    {
        // Instantiate and launch the shell.
        Rigidbody shellInstance = Instantiate(shellRigidbody, fireTransform.position, fireTransform.rotation);
        shellInstance.velocity = fireTransform.forward * CurrentLaunchForce;
        shootingAudio.clip = fireClip;
        shootingAudio.Play();

        aimSlider.value = minLaunchForce;
    }

    public void SetupRequirementStatForAI()
    {
        CurrentLaunchForce = GenerateRandomPowerForce();
    }

    private void HandleTankPowerForce(int angle, float power)
    {
        CurrentLaunchForce = Math.Max(minLaunchForce, maxLaunchForce * power/100);
    }

    private float GenerateRandomPowerForce()
    {
        return Random.Range(minLaunchForce, maxLaunchForce);
    }
}