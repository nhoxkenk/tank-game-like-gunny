using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    public TankFaction faction;
    public bool isMoving;
    public bool haveFuelLeft = true;
}
