﻿using UnityEngine;
using UnityEngine.InputSystem;

public class TankMovement : MonoBehaviour
{
    public int playerNumber = 1;         
    public float speed = 12f;            
    public float turnSpeed = 180f;       
    public AudioSource movementAudio;    
    public AudioClip engineIdling;       
    public AudioClip engineDriving;      
    public float pitchRange = 0.2f;
    public PlayerInput playerInput;
   
    private string movementAxisName;     
    private string turnAxisName;         
    private Rigidbody rigidbodyTank;     
    private Tank tank;
    private float movementInputValue;    
    private float turnInputValue;        
    private float originalPitch;

    private void Awake()
    {
        rigidbodyTank = GetComponent<Rigidbody>();
        tank = GetComponent<Tank>();
    }

    private void OnEnable()
    {
        rigidbodyTank.isKinematic = false;
        movementInputValue = 0f;
        turnInputValue = 0f;
    }

    public void OnMovement(InputAction.CallbackContext value)
    {
        if(tank.faction == TankFaction.Enemy)
            return;
        if (!tank.haveFuelLeft)
        {
            movementInputValue = 0;
            tank.isMoving = false;
            return;
        }
        Vector2 inputMovement = value.ReadValue<Vector2>();
        movementInputValue = inputMovement.x;
        tank.isMoving = movementInputValue == 0f ? false : true;
    }


    private void OnDisable ()
    {
        rigidbodyTank.isKinematic = true;
    }


    private void Start()
    {
        movementAxisName = "Vertical" + playerNumber;
        turnAxisName = "Horizontal" + playerNumber;

        originalPitch = movementAudio.pitch;
    }
    
    private void Update()
    {
        // Store the player's input and make sure the audio for the engine is playing.
        //movementInputValue = Input.GetAxis(movementAxisName);
        turnInputValue = Input.GetAxis(turnAxisName);

        EngineAudio();
    }


    private void EngineAudio()
    {
        // Play the correct audio clip based on whether or not the tank is moving and what audio is currently playing.
        if(Mathf.Abs(movementInputValue) < 0.1f && Mathf.Abs(turnInputValue) < 0.1f)
        {
            if(movementAudio.clip == engineDriving)
            {
                movementAudio.clip = engineIdling;
                movementAudio.pitch = Random.Range(originalPitch - pitchRange, originalPitch + pitchRange);
                movementAudio.Play();
            }
        }
        else
        {
            if (movementAudio.clip == engineIdling)
            {
                movementAudio.clip = engineDriving;
                movementAudio.pitch = Random.Range(originalPitch - pitchRange, originalPitch + pitchRange);
                movementAudio.Play();
            }
        }
    }


    private void FixedUpdate()
    {
        // Move and turn the tank.
        Move();
        Turn();
    }


    private void Move()
    {
        Vector3 movement = transform.forward * movementInputValue * speed * Time.deltaTime;
        rigidbodyTank.MovePosition(rigidbodyTank.position + movement);
    }


    private void Turn()
    {
        float turn = turnInputValue * turnSpeed * Time.deltaTime;
        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);
        rigidbodyTank.MoveRotation(rigidbodyTank.rotation * turnRotation);
    }
}