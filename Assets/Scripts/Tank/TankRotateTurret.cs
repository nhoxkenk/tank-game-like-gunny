﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class TankRotateTurret : MonoBehaviour
{
    #region
    //Vector3 pointDirection = new Vector3(tankTurret.transform.position.x, Input.mousePosition.y, Input.mousePosition.z);
    //tankTurret.transform.LookAt(pointDirection);

    //Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    //mouseWorldPosition.z = 0;
    //tankTurret.transform.LookAt(mouseWorldPosition);

    //Vector3 vector = GetMousePosition(Input.mousePosition) - base.transform.position;
    //vector.x = 0f;
    //float x = Mathf.Atan2(vector.y, vector.z) * 57.29578f;
    //Quaternion b = Quaternion.Euler(x, 0f, 0f);
    //tankTurret.transform.rotation = Quaternion.Slerp(tankTurret.transform.rotation, b, rotationSpeed * Time.deltaTime); 

    //Vector2 pointDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - tankTurret.position;
    //float angle = Mathf.Atan2(pointDirection.y, pointDirection.x) * Mathf.Rad2Deg;
    //Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    //tankTurret.rotation = rotation;

    //Vector3 mouse = Input.mousePosition;
    //Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(mouse);
    //mouseWorld.z = 0;
    //mouseWorld.x = tankTurret.position.x;
    //Vector3 forward = mouseWorld - tankTurret.position;
    //tankTurret.rotation = Quaternion.LookRotation(forward, Vector3.up);

    //Debug.DrawLine(tankTurret.position, forward, Color.white, Time.deltaTime);
    #endregion

    public Transform tankTurret;
    public bool FacingRight = true;
    public static event Action<int, float> OnJoystickValueChanged;

    private Vector2 GameobjectRotation;
    private float GameobjectRotation2;
    private float GameobjectRotation3;
    private Quaternion initialTurretRotation;
    private Tank tank;

    private void Start()
    {
        initialTurretRotation = tankTurret.localRotation;
        tank = GetComponent<Tank>();
    }

    private void Flip()
    {
        // Flips the player.
        FacingRight = !FacingRight;

        transform.Rotate(0, 180, 0);
    }

    public void RotateRandomAngle()
    {
        float angle = Random.Range(0, -45);
        tankTurret.rotation = Quaternion.Euler(angle, 0, 0);
    }

    public void OnRotate(InputAction.CallbackContext value)
    {
        if (tank.faction != TankFaction.Player)
            return;
        GameobjectRotation = value.ReadValue<Vector2>();

        if(GameobjectRotation == Vector2.zero)
        {
            initialTurretRotation = tankTurret.rotation;
        }

        GameobjectRotation3 = GameobjectRotation.x;

        if (FacingRight)
        {
            //Rotates the object if the player is facing right
            GameobjectRotation2 = GameobjectRotation.x + GameobjectRotation.y * -90;
            Quaternion desiredRotation = Quaternion.Euler(GameobjectRotation2, 180f, 0f);
            tankTurret.rotation = Quaternion.RotateTowards(tankTurret.rotation, desiredRotation, 100 * Time.deltaTime);
        }
        else
        {
            //Rotates the object if the player is facing left
            GameobjectRotation2 = GameobjectRotation.x + GameobjectRotation.y * 90;
            Quaternion desiredRotation = Quaternion.Euler(-GameobjectRotation2, 0f, 0f);
            tankTurret.rotation = Quaternion.RotateTowards(tankTurret.rotation, desiredRotation, 100 * Time.deltaTime);
        }
        if (GameobjectRotation3 < 0 && FacingRight)
        {
            // Executes the void: Flip()
            Flip();
        }
        else if (GameobjectRotation3 > 0 && !FacingRight)
        {
            // Executes the void: Flip()
            Flip();
        }
        if(GameobjectRotation2 != 0)
        {
            float normalizedAngle = NormalizeAngle180(tankTurret.eulerAngles.x);
            OnJoystickValueChanged?.Invoke((int)normalizedAngle, (float)Math.Round(GameobjectRotation.x, 2) * 100f);
        } 
    }

    float NormalizeAngle180(float angle)
    {
        angle %= 360f;
        if (angle > 180f)
        {
            angle -= 360f;
        }
        return -angle;
    }
}
