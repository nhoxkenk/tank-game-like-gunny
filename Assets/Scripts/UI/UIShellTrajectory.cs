using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShellTrajectory : MonoBehaviour
{
    public GameObject pointPrefabs;
    [SerializeField]
    private Transform start;
    [SerializeField]
    private Transform desination;
    [SerializeField] private TankShooting playerTankShooting;
    [SerializeField] private GameObject[] points;
    [SerializeField] private float launchForce;

    private void Start()
    {
        for(int i = 0; i < points.Length; i++)
        {
            points[i] = Instantiate(pointPrefabs, transform.position, Quaternion.identity);
        }
        start = playerTankShooting.fireTransform;
    }

    private void Update()
    {
        for (int i = 0; i < points.Length; i++)
        {
            points[i].transform.position = SetupEachPointPosition(i * 0.15f);
        }
    }

    private Vector3 SetupEachPointPosition(float time)
    {
        launchForce = playerTankShooting.CurrentLaunchForce;
        Vector3 direction = desination.position - start.position;
        Vector3 gravityComponent = Physics.gravity * (time * time);
        Vector3 pointPos = start.position + (direction.normalized * launchForce * time) + (0.5f * gravityComponent);
        return pointPos;
    }
}
